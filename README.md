# Sailing Downstream mentorship coding challenge

This is a simple script for a mentorship task for the RISC-V International and Linux Foundation.

This is project using only Python's standard libraries on purpose.

### How to use

1. Make sure you have Python 3.11 installed

2. Clone the repository

3. Open Python's REPL and do the following:

    ```
    from item_selector import select_items

    select_items([2, 3, 4, 5, 2, 3, 4, 5, 6, 7]) # this list is just an example input
    ```


### Testing

To run tests, type in your terminal:

    python item_selector_test.py

I tried to use property testing for the first time but was not successful.

I could have used testing by gathering a huge amount of numbers that are multiples of 2 and 3 then check if these numbers are not in the result. But I am pretty sure this is working as it should.
