def select_items(items):
    if len(items) % 10 != 0:
        raise Exception("The list is not a multiple of 10 in length.")

    selected_items = []

    for i in range(len(items)):
        if (i % 2 != 0) and (i % 3 != 0):
            selected_items.append(items[i])

    return selected_items
