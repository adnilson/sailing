import unittest, pdb
from item_selector import select_items

class TestIntegerSelector(unittest.TestCase):
    def test_array_multiple_of_10(self):
        with self.assertRaises(Exception) as cm:
            select_items([2, 3, 44, 5, 6, 6])

        self.assertTrue('The list is not a multiple of 10 in length.' in str(cm.exception))

    def test_length_of_10(self):
        self.assertEqual(select_items([2, 3, 4, 5, 2, 3, 4, 5, 6, 7]), [3, 3, 5])

    def test_length_of_20(self):
        self.assertEqual(select_items(range(20)), [1, 5, 7, 11, 13, 17, 19])

    def test_length_of_30(self):
        self.assertEqual(select_items(range(30)), [1, 5, 7, 11, 13, 17, 19, 23, 25, 29])

if __name__ == '__main__':
    unittest.main()